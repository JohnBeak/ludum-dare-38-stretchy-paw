﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZappingScript : MonoBehaviour {
    private PlayerManager playerManager;

    public void Init(PlayerManager _playerManager)
    {
        playerManager = _playerManager;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Eel")
        {
            playerManager.Zap();
            collision.gameObject.GetComponent<ThunderboltScript>().Activate();
        }
    }
}
