﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTileScript : MonoBehaviour {
    public class Coords
    {
        public int x, y;

        public Coords()
        {
            x = 0;
            y = 0;
        }
        public Coords(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public Coords Above()
        {
            return new Coords(x, y - 1);
        }
        public Coords Below()
        {
            return new Coords(x, y + 1);
        }
        public Coords Left()
        {
            return new Coords(x - 1, y);
        }
        public Coords Right()
        {
            return new Coords(x + 1, y);
        }

        // Why, C#, why? I just wanted to use Dictionary to avoid having to set array size for my grid and it wouldn't match keys when their values match. PHP is easier to use because you never have to declare variables. Aaah!
        // I don't even know how interfaces work
        public class EqualityComparer : IEqualityComparer<Coords>
        {

            public bool Equals(Coords first, Coords second)
            {
                return first.x == second.x && first.y == second.y;
            }

            public int GetHashCode(Coords first)
            {
                return first.x * 16777216 + first.y;
            }

        }
    }

    Coords coords;

    private GameManager gameManager;
    private SpriteRenderer spriteRenderer;

    public Sprite[] tiles;

    // Unity doesn't run Start() immediately after instantiating, giving me a lot of pain figuring out why certain values weren't set or were rewritten. This happened many, many times. Unity pls.
    public void Init(Coords _coords)
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        coords = _coords;
    }

    public void ShapeFromNeighbours() {
        if (!gameManager) { return; }
        int offset = 0;
        GameObject neighbour;
        neighbour = gameManager.GridObjectAtCoords(coords.Above());
        if (!(neighbour == null) && (tag == neighbour.tag || (tag == "Paw" && neighbour.tag == "Player"))) { offset += 8; }
        neighbour = gameManager.GridObjectAtCoords(coords.Below());
        if (!(neighbour == null) && (tag == neighbour.tag || (tag == "Paw" && neighbour.tag == "Player"))) { offset += 4; }
        neighbour = gameManager.GridObjectAtCoords(coords.Left());
        if (!(neighbour == null) && (tag == neighbour.tag || (tag == "Paw" && neighbour.tag == "Player"))) { offset += 2; }
        neighbour = gameManager.GridObjectAtCoords(coords.Right());
        if (!(neighbour == null) && (tag == neighbour.tag || (tag == "Paw" && neighbour.tag == "Player"))) { offset += 1; }
        spriteRenderer.sprite = tiles[offset];
    }

    public Coords GetCoords()
    {
        return coords;
    }
}
