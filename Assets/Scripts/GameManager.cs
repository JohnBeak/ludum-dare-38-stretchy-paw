﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public TextAsset level;
    public GameObject wallPrefab;
    public GameObject fishPrefab;
    public GameObject eelPrefab;
    public GameObject playerPrefab;
    public GameObject pawPrefab;
    public GameObject kittyPrefab;

    public float tileSize = 1f;

    public GameObject HUDCatLife, HUDFishCount, HUDMessageText, HUDPressEscToContinue;

    public int catLife = 9, fishCount = 0;

    private Dictionary<GridTileScript.Coords, GameObject> grid;
    private List<GameObject> movableObjects;
    private GameObject player;
    private List<GameObject> pawTiles;
    private GameObject kitty;

    private void Start()
    {
        grid = new Dictionary<GridTileScript.Coords, GameObject>(new GridTileScript.Coords.EqualityComparer());
        movableObjects = new List<GameObject>();
        pawTiles = new List<GameObject>();
        
        GenerateMap();
    }

    void ClearMap()
    {
        if (grid == null) { grid = new Dictionary<GridTileScript.Coords, GameObject>(new GridTileScript.Coords.EqualityComparer()); }
        foreach (KeyValuePair<GridTileScript.Coords, GameObject> gridIndexAndObject in grid)
        {
            GameObject spawnedObject = gridIndexAndObject.Value;
            Destroy(spawnedObject);
        }
        // TODO: fix
    }

    void GenerateMap() {
        ClearMap();

        if (level == null) { return; }

        int x = 0, y = 0;
        Vector3 position;

        foreach (string line in level.text.Split(new[] { System.Environment.NewLine }, System.StringSplitOptions.RemoveEmptyEntries))
        {
            foreach (char character in line)
            {
                position = GameCoordsToWorldCoords(new GridTileScript.Coords(x, y));
                switch (character)
                {
                    case ('X'):
                        GameObject wall = Instantiate(wallPrefab, position, Quaternion.identity);
                        GridTileScript gridTileScript = wall.GetComponent<GridTileScript>();
                        gridTileScript.Init(new GridTileScript.Coords(x, y));
                        grid[new GridTileScript.Coords(x, y)] = wall;
                        break;
                    case ('P'):
                        GridTileScript.Coords playerSpawnCoords = new GridTileScript.Coords(x, y - 1);
                        player = Instantiate(playerPrefab, GameCoordsToWorldCoords(playerSpawnCoords), Quaternion.identity);
                        PlayerManager playerManager = player.GetComponent<PlayerManager>();
                        grid[playerSpawnCoords] = player;
                        playerManager.Init(new GridTileScript.Coords(x, y - 1));
                        player.GetComponent<ZappingScript>().Init(playerManager);
                        //playerManager.Extend(new GridTileScript.Coords(x, y));
                        kitty = Instantiate(kittyPrefab, new Vector3(position.x + 0.5f, position.y + 1.5f, position.z), Quaternion.identity);
                        kitty.GetComponent<EatingScript>().Init(playerManager);
                        //grid[new GridTileScript.Coords(x, y - 1)] = kitty;
                        break;
                    case ('<'):
                        movableObjects.Add(Instantiate(fishPrefab, position, Quaternion.identity));
                        fishCount++;
                        UpdateHUD();
                        break;
                    case ('>'):
                        movableObjects.Add(Instantiate(fishPrefab, position, Quaternion.Euler(0f, 180f, 0f)));
                        fishCount++;
                        UpdateHUD();
                        break;
                    case ('z'):
                        movableObjects.Add(Instantiate(eelPrefab, position, Quaternion.identity));
                        break;
                    case ('s'):
                        movableObjects.Add(Instantiate(eelPrefab, position, Quaternion.Euler(0f, 180f, 0f)));
                        break;
                }
                x++;
            }
            y++;
            x = 0;
        }
        foreach (KeyValuePair<GridTileScript.Coords, GameObject> gridItem in grid)
        {
            GridTileScript gridTileScript = gridItem.Value.GetComponent<GridTileScript>();
            if (!gridTileScript) { continue; }
            gridItem.Value.GetComponent<GridTileScript>().ShapeFromNeighbours();
        }
    }

    public void LoseLife()
    {
        catLife--;
        if (catLife <= 0)
        {
            catLife = 0;
            GameOver();
        }
        UpdateHUD();
    }

    void GameOver()
    {
        HUDMessageText.GetComponent<Text>().text = "Game Over";
        player.GetComponent<PlayerManager>().EndGame();
        HUDPressEscToContinue.SetActive(true);
    }

    void Winner()
    {
        HUDMessageText.GetComponent<Text>().text = "Winner!";
        player.GetComponent<PlayerManager>().EndGame();
        HUDPressEscToContinue.SetActive(true);
    }

    public void UpdateHUD()
    {
        HUDCatLife.GetComponent<Text>().text = catLife.ToString();
        HUDFishCount.GetComponent<Text>().text = fishCount.ToString();
    }

    public void SetPausedAllFish(bool value)
    {
        for (int i = 0; i < movableObjects.Count; i++)
        {
            if (movableObjects[i])
            {
                movableObjects[i].GetComponent<FishScript>().SetPaused(value);
            }
        }
    }

    public Vector3 GameCoordsToWorldCoords(GridTileScript.Coords coords)
    {
        return new Vector3((float)coords.x * tileSize, (float)-coords.y * tileSize);
    }

    public GameObject GridObjectAtCoords(GridTileScript.Coords coords)
    {
        if (grid == null) { return null; }
        if (grid.ContainsKey(coords)) { return grid[coords]; }
        return null;
    }

    public GameObject LastPaw()
    {
        if (pawTiles == null || pawTiles.Count == 0) { return null; }
        return pawTiles[pawTiles.Count - 1];
    }

    public void MovePlayer(GridTileScript.Coords target)
    {
        if (grid[player.GetComponent<PlayerManager>().CurrentCoords()] == player) { grid.Remove(player.GetComponent<PlayerManager>().CurrentCoords()); }
        grid[target] = player;
    }

    public void SpawnPaw(GridTileScript.Coords target)
    {
        GameObject newPaw = Instantiate(pawPrefab, GameCoordsToWorldCoords(target), Quaternion.identity);
        GridTileScript newPawScript = newPaw.GetComponent<GridTileScript>();
        newPawScript.Init(target);
        newPawScript.ShapeFromNeighbours();
        grid[target] = newPaw;
        newPaw.GetComponent<ZappingScript>().Init(player.GetComponent<PlayerManager>());
        pawTiles.Add(newPaw);
    }

    public void RemovePaw()
    {
        GameObject lastpaw = LastPaw();
        if (lastpaw == null) { return; }
        grid.Remove(lastpaw.GetComponent<GridTileScript>().GetCoords());
        pawTiles.Remove(lastpaw);
        Destroy(lastpaw);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) { UnityEngine.SceneManagement.SceneManager.LoadScene("Main"); }
    }

    private void FixedUpdate()
    {
        if (fishCount <= 0) { Winner(); }
    }
}
