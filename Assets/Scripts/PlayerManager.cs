﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public GameObject paw;

    public KeyCode keyUp = KeyCode.UpArrow;
    public KeyCode keyDown = KeyCode.DownArrow;
    public KeyCode keyLeft = KeyCode.LeftArrow;
    public KeyCode keyRight = KeyCode.RightArrow;

    public float animationSpeed = 0.1f;

    bool inputEnabled, isAnimating, isExtending, isRetracting, isPaused, isHoldingFish, isZapped, inMouth, gameOver;
    GridTileScript.Coords currentCoords, nextCoords;
    float animationPhase;

    private GameManager gameManager;

    public void Init(GridTileScript.Coords _coords)
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();

        inputEnabled = true;
        isAnimating = false;
        isExtending = false;
        isRetracting = false;
        isPaused = false;
        isHoldingFish = false;
        isZapped = false;
        inMouth = false;
        gameOver = false;

        currentCoords = _coords;
        nextCoords = currentCoords;

        animationPhase = 0f;

        SetPawRotation(new GridTileScript.Coords(0, 0), new GridTileScript.Coords(0, 1));
    }

    public void EndGame()
    {
        gameOver = true;
    }

    public void Extend(GridTileScript.Coords target)
    {
        isExtending = true;
        inputEnabled = false;
        AnimatePlayer(target);
        gameManager.SpawnPaw(currentCoords);
        gameManager.MovePlayer(target);
        SetPawRotation(currentCoords, target);
    }

    void Retract()
    {
        isRetracting = true;
        inputEnabled = false;
        GridTileScript.Coords lastPawCoords = gameManager.LastPaw().GetComponent<GridTileScript>().GetCoords();
        AnimatePlayer(lastPawCoords);
        SetPawRotation(lastPawCoords, currentCoords);
    }

    void AnimatePlayer(GridTileScript.Coords target)
    {
        isAnimating = true;
        nextCoords = target;
    }

    void SetPawRotation(GridTileScript.Coords source, GridTileScript.Coords target)
    {
        // lousy, lousy code
        if (source.x == target.x)
        {
            if (source.y > target.y) { paw.transform.rotation = Quaternion.Euler(0f, 0f, 0f); } // up
            else { paw.transform.rotation = Quaternion.Euler(0f, 0f, 180f); } // down
        }
        else
        {
            if (source.x > target.x) { paw.transform.rotation = Quaternion.Euler(0f, 0f, 90f); } // left
            else { paw.transform.rotation = Quaternion.Euler(0f, 0f, -90f); } // right
        }
    }

    private void Update()
    {
        // handle input
        if (!gameOver && inputEnabled)
        {
            if (Input.anyKey)
            {
                GridTileScript.Coords targetCoords = null;
                bool up = false;

                if (Input.GetKey(keyUp)) { targetCoords = currentCoords.Above(); up = true; }
                else if (Input.GetKey(keyDown)) { targetCoords = currentCoords.Below(); }
                else if (Input.GetKey(keyLeft)) { targetCoords = currentCoords.Left(); }
                else if (Input.GetKey(keyRight)) { targetCoords = currentCoords.Right(); }

                if (targetCoords == null) { return; } // not pressing any keys relevant to player

                GameObject target = gameManager.GridObjectAtCoords(targetCoords);

                if (target == null)
                {
                    Extend(targetCoords);
                }
                else if (target.tag == "Wall")
                {
                    if (up) { Retract(); }
                }
                else if (target.tag == "Paw")
                {
                    GridTileScript.Coords cSharpPls = gameManager.LastPaw().GetComponent<GridTileScript>().GetCoords();
                    if (targetCoords.x == cSharpPls.x && targetCoords.y == cSharpPls.y) // == doesn't work
                    {
                        Retract();
                    }
                }
                else if (target.tag == "Mouth")
                {
                    // TODO: eating animation
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (isAnimating)
        {
            animationPhase += animationSpeed;

            // check if animation ended
            if (animationPhase >= 1f)
            {
                if (isExtending)
                {
                    gameManager.GridObjectAtCoords(currentCoords).GetComponent<GridTileScript>().ShapeFromNeighbours();
                    isExtending = false;
                }
                if (isRetracting)
                {
                    gameManager.RemovePaw();
                    gameManager.MovePlayer(nextCoords);
                    GameObject lastPaw = gameManager.LastPaw();
                    if (lastPaw != null)
                    {
                        lastPaw.GetComponent<GridTileScript>().ShapeFromNeighbours();
                        SetPawRotation(lastPaw.GetComponent<GridTileScript>().GetCoords(), nextCoords);
                    }
                    isRetracting = false;

                }

                currentCoords = nextCoords;
                animationPhase = 0f;

                // TODO: spawn something?
                // TODO: animate more?
                isAnimating = false;
                inputEnabled = true;


            }

            // update position in scene
            transform.position = (1f - animationPhase) * gameManager.GameCoordsToWorldCoords(currentCoords) + animationPhase * (gameManager.GameCoordsToWorldCoords(nextCoords));
        }

        if (!isAnimating)
        {
            if (inMouth)
            {
                inMouth = false;
                Extend(new GridTileScript.Coords(currentCoords.x, currentCoords.y + 1));
            }

            if (isZapped)
            {
                Retract();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Fish")
        {
            if (isHoldingFish)
            {
                if (isAnimating)
                {
                    if (isExtending)
                    {
                        CancelExtending();
                    }
                }
            }
            else if (!isZapped && !collision.gameObject.GetComponent<FishScript>().isBeingEaten)
            {
                CatchFish(collision.gameObject);
            }
        }
        else if (collision.tag == "Mouth")
        {
            inMouth = true;
            gameManager.SetPausedAllFish(false);
            isZapped = false;
        }
    }

    public void Zap()
    {
        isZapped = true;
        // play animation - handled in ZappingScript
        // TODO: pause all fish
        gameManager.SetPausedAllFish(true);
        if (isHoldingFish) { ReleaseFish(); }
        // retract all - handled in FixedUpdate
        gameManager.LoseLife();
    }

    void ReleaseFish()
    {
        GameObject fish = paw.transform.Find("Fish(Clone)").gameObject;
        fish.GetComponent<FishScript>().isCaught = false;
        fish.transform.rotation = Quaternion.identity;
        fish.transform.position = gameManager.GameCoordsToWorldCoords(currentCoords);
        fish.transform.parent = null;
        isHoldingFish = false;
    }

    void CancelExtending()
    {
        // TODO: fix fish bug
        GridTileScript.Coords temp = nextCoords;
        nextCoords = currentCoords;
        currentCoords = temp;
        animationPhase = 1f - animationPhase;
        isExtending = false;
        isRetracting = true;
    }

    void CatchFish(GameObject fish)
    {
        fish.transform.parent = paw.transform;
        fish.transform.localPosition = new Vector3(0f, 0f, -1f);
        fish.transform.rotation = paw.transform.rotation;
        fish.GetComponent<FishScript>().isCaught = true;
        isHoldingFish = true;
    }

    public void EatFish()
    {
        isHoldingFish = false;
        gameManager.fishCount--;
        gameManager.UpdateHUD();
        // the rest is handled in eating manager
    }

    public GridTileScript.Coords CurrentCoords()
    {
        return currentCoords;
    }
}
