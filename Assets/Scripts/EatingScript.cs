﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatingScript : MonoBehaviour {
    public Vector3 fishStartingPosition = new Vector3(-0.08f, -0.62f, -1f);

    private PlayerManager playerManager;

    public void Init(PlayerManager _playerManager)
    {
        playerManager = _playerManager;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Fish")
        {
            EatFish(collision.gameObject);
        }
    }

    void EatFish(GameObject fish)
    {
        fish.transform.parent = transform;
        fish.transform.position = transform.position + fishStartingPosition;
        fish.transform.rotation = Quaternion.Euler(0f, 0f, -90f);
        fish.GetComponent<FishScript>().isCaught = false;
        fish.GetComponent<FishScript>().isBeingEaten = true;
        playerManager.EatFish();
    }
}
