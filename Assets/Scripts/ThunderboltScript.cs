﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderboltScript : MonoBehaviour {
    public GameObject thunderbolt;

    public float interval = 1f;

    bool showing;
    float activeFor;
    

    private void Start()
    {
        showing = false;
        activeFor = 0f;
    }

    void FixedUpdate () {
        if (activeFor >= interval)
        {
            Deactivate();
        }
        if (showing)
        {
            activeFor += Time.deltaTime;
        }
	}

    public void Activate()
    {
        showing = true;
        thunderbolt.SetActive(true);
        activeFor = 0f;
    }

    void Deactivate()
    {
        showing = false;
        thunderbolt.SetActive(false);
        activeFor = 0f;
    }
}
