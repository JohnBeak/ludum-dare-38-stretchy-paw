﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishScript : MonoBehaviour {
    public float speed = 0.1f, eatingSpeed = 0.005f;

    public bool isCaught, isPaused, isBeingEaten;

    private void Start()
    {
        isCaught = false;
        isPaused = false;
    }

    void FixedUpdate () {
        if (isBeingEaten) { speed = eatingSpeed; }
        if (!isPaused && !isCaught) { transform.Translate(-speed, 0f, 0f); }
	}

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Throat") { Destroy(this.gameObject); }
        // TODO: rethink
        if (isCaught || isBeingEaten) { return; }
        //if (target.tag == "Neighbour Detector") { return; }

        // turn around when you hit something
        transform.rotation = Quaternion.Euler(0f, Mathf.Round(1f - transform.rotation.y) * 180f, 0f);
    }

    public void SetPaused(bool value)
    {
        isPaused = value;
    }
}
